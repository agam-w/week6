const express = require('express')
const app = express()
var cors = require('cors')

const mongoose = require('mongoose')
const morgan = require('morgan')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(morgan('tiny'))

mongoose.connect('mongodb://localhost/week6', { useNewUrlParser: true, useCreateIndex: true })
  .then(() => console.log("connect DB"))

  const router = require('./routers');
  app.use('/api', router);

  app.get('/', (req, res) => {
    res.status(200).json({
      success: true,
      message: `Welcome to week6!`
    });
  });

  const port = 6000
  app.listen(port, () => {
    console.log(`Server Started at ${Date()}!`)
    console.log(`Listening on port ${port} in database week5!`)
  })