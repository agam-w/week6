const User = require('../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const {success, error} = require('../helpers/response')

module.exports = {
    create(req, res){
        User.create(req.body, (err, data)=>{
            if (err) return res.status(422).json(error(err))
            res.status(201).json(success(data, "User created"))
        })
    },
    login(req, res){

        User.findOne({email:req.body.email}).then(function(user){
            if(!user){
                return res.status(400).json(error("invalid user"))
            } else {
                bcrypt.compare(req.body.password, user.password, function (err, result) {
                    if (result == true) {
                        jwt.sign({ _id: user._id }, 'xxx', function(err, token) {
                            res.status(200).json(success(token, "token created"));
                        });
                    } else {
                        res.status(403).json(error('Incorrect password'));
                    }

                })
            }
        })
    }
}