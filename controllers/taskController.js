const Task = require('../models/task')
const User = require('../models/user')
const {success, error} = require('../helpers/response')

module.exports = {
    create(req, res){
        Task.create(req.body, function (err, data){

            if (err) return res.status(400).json(error("Can't make post"))
            User.findById(req.user._id, (err, user)=>{

                if (err) return res.status(400).json(error("can't find user"))
                user.tasks.push(data)
                user.save()
                data.user=req.user._id
                data.save()
            })
            res.status(201).json(success(data, "post created"))

        })
    },
    taskDone(req, res){
        Task.findOne({_id: req.params.id}, (err, data)=>{
            if (!data || data.user._id != req.user._id) return res.status(404).json(error("invalid task id"))
            if (data.isDone==true) return res.status(400).json(error("this task already done"))
            else Task.findOneAndUpdate({_id: data._id}, {isDone: true}, (err, data)=>{
                res.status(200).json(success(data, "task finished"))
            })
        })
    },
    deleteTask(req, res){
        Task.findOne({_id: req.params.id}, (err, data)=>{
            if (!data || data.user._id != req.user._id) return res.status(404).json(error("invalid task id"))
            if (data.isDone!==true) return res.status(400).json(error("this task has not been completed"))
            else Task.findOneAndDelete({_id: data._id}, (err, data)=>{
                res.status(200).json(success(data, "task has been delete"))
            })
        })
    }

}