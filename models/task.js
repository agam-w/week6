const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = require('./user')

const taskSchema = new Schema({

    title    :{type   :'string',
              required: true},
    level    :{type  :'number',
              min: 1,   max: 5,
              required: true},
    point    :{type  : 'number'},
    isDone   :{type   : 'boolean',
              default : false},
    user     :{type   : Schema.Types.ObjectId, 
              ref     : 'User'}

  });

taskSchema.pre('save', function (next){
    var task = this
    task.point = task.level*2
        next()
})

taskSchema.post('findOneAndUpdate', function (data, next){
    var task = data
    User.findById(task.user._id,(err, data)=>{
        if(err) return next(err)
        data.totalPoint += task.point
        data.save()
        next()
    })
})

taskSchema.post('findOneAndDelete', function (data, next){
    var task = data
    User.findById(task.user._id,(err, user)=>{
        if(err) return next(err)
        user.totalPoint -= task.point
        user.save()
        User.update({ _id: user._id }, { $pullAll: { tasks: [task._id] } }, 
            { safe: true, multi:true }, function(err, obj) {
                next()
            })
        
    })
})

module.exports = mongoose.model('Task', taskSchema);
