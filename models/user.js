const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')
const saltRounds = 10;
var uniqueValidator = require('mongoose-unique-validator');
require('mongoose-type-email')

const userSchema = new Schema({

    username    : {type   :'string',
                  required: true,
                  unique  : true},
    email       : {type   : mongoose.SchemaTypes.Email,
                  required: true,
                  unique  : true},
    password    : {type   : 'string',
                  required: true},
    tasks       : [{type  : Schema.Types.ObjectId, 
                  ref     : 'Task'}],
    totalPoint  : {type   : 'number',
                  default : 0}

  });

userSchema.plugin(uniqueValidator);

userSchema.pre('save', function(next){
    var user = this
    bcrypt.hash(user.password, saltRounds, (err, hash)=>{
        if(err) return next(err)
        user.password = hash
        next()
    })
})

module.exports = mongoose.model('User', userSchema)