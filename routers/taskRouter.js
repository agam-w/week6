const router = require('express').Router()
const taskController = require('../controllers/taskController')
const authenticate = require('../middlewares/authenticate')

router.post('/', authenticate, taskController.create);
router.put('/:id', authenticate, taskController.taskDone)
router.delete('/:id', authenticate, taskController.deleteTask)
module.exports = router