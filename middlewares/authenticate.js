const jwt = require('jsonwebtoken');
const {success, error} = require('../helpers/response.js')
const User = require('../models/user.js')

module.exports =  function (req, res, next){

    const token1 = req.header('authorization')
    if(!token1) return res.status(401).json(error("can't put from header"))

    const token0 = req.header('authorization').split(" ")
    const token = token0[1]
 
    try {
        const verified = jwt.verify(token, 'xxx')
        req.user = verified
        
        User.findById(req.user._id)
            .exec(function (err, user) {
                if (!user) return res.status(403).json(error("there is no user found"));
                next()
        });
        
    }

    catch (err){
        res.status(401).json(error("Invalid Token"))
    }
}